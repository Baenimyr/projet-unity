using UnityEngine;

public class Modeles : MonoBehaviour {
	[Header("Bâtiments")]

	[SerializeField]
	public GameObject herbe;

	[SerializeField]
	public GameObject route;

	[SerializeField]
	public GameObject batiment_bas;

	[SerializeField]
	public GameObject[] batiments_hauts;

	[Header("Arbre")]
	public GameObject arbre = null;

	[Header("Habitants")]
	public GameObject citoyen = null;
}