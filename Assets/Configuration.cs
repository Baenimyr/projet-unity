using UnityEngine;

public class Configuration : MonoBehaviour {
	[Header("Génération")]

	[SerializeField]
	public float largeur = 40;
	
	/// <summary>Nombre de terrain par unité de surface dans les quartiers</summary>
	[SerializeField]
	public float surfaceBatiments = 50f;

	[Header("Population")]
	[Tooltip("Nombre d'habitant par m2 dans\nles quartiers résidentiels")]
	public float popResidentiel = 0.1f;
	[Tooltip("Nombre d'habitant par m2 dans\nles quartiers résidentiels dense")]
	public float popDense = 1.5f;
}