using UnityEngine;
using System.Collections.Generic;

namespace geometrie {

public class Maillage {
	protected List<Vector3> sommets;
	protected List<int[]> connexions;

	public Maillage() {
		this.sommets = new List<Vector3>();
		this.connexions = new List<int[]>();
	}

	public Maillage(List<UnityEngine.Vector3> sommets) {
		this.sommets = new List<Vector3>(sommets);
		this.connexions = new List<int[]>();
	}

	public int Count {
		get { return this.sommets.Count; }
	}

	public Vector3 sommet(int index) {
		return this.sommets[index];
	}

	public int ajoutSommet(Vector3 v) {
		int i = this.sommets.LastIndexOf(v);
		if (i == -1) {
			this.sommets.Add(v);
			return this.sommets.Count - 1;
		}
		return i;
	}

	public void ajoutTriangle(int i, int j, int k) {
		this.connexions.Add(new int[]{i, j, k});
	}

	public void ajoutTriangle(Vector3 i, Vector3 j, Vector3 k) {
		int i_id = this.ajoutSommet(i);
		int j_id = this.ajoutSommet(j);
		int k_id = this.ajoutSommet(k);
		
		this.ajoutTriangle(i_id, j_id, k_id);
	}

	public void apply(Mesh mesh) {
		Vector3[] vertices = new Vector3[sommets.Count];
		int[] triangles = new int[connexions.Count * 3];

		for (int i = 0; i < sommets.Count; i++) {
			vertices[i] = sommets[i];
		}
		for (int j = 0; j < connexions.Count; j++) {
			triangles[j * 3 + 0] = connexions[j][0];
			triangles[j * 3 + 1] = connexions[j][1];
			triangles[j * 3 + 2] = connexions[j][2];
		}

		mesh.Clear();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
	}
}

}