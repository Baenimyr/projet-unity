using UnityEngine;

namespace geometrie {
public struct Triangle : IPolygone {
	public readonly Vector2 A, B, C;

	public Triangle(Vector2 s1, Vector2 s2, Vector2 s3) {
		this.A = s1;
		this.B = s2;
		this.C = s3;
	}

	public bool utilise(Vector2 p) {
		return p == A || p == B || p == C;
	}

	public int indexOf(Vector2 s1, Vector2 s2) {
		if (s1 == A && s2 == B) return 0;
		else if (s1 == B && s2 == C) return 1;
		else if (s1 == C && s2 == A) return 2;
		else return -1;
	}

	public int indexOf(Vector2 s) {
		if (A == s) return 0;
		else if (B == s) return 1;
		else if (C == s) return 2;
		else return -1;
	}

	public Vector2 this[int index] { get {
		switch(index) {
			case 0: return A;
			case 1: return B;
			case 2: return C;
			default: throw new System.IndexOutOfRangeException(index + " invalide pour un triangle");
		}
	}}

	public float aire() {
		return (A.x * B.y - A.y * B.x + B.x * C.y - B.y * C.x) / 2;
	}

	public int size() {
		return 3;
	}

	public bool contiens(Vector2 x) {
		Vector2 v1 = (B - A).normalized;
		Vector2 v2 = (C - A).normalized;
		float a = Vector2.Dot(x - A, v1);
		float b = Vector2.Dot(x - A, v2);

		return 0 <= a && a <= 1 && 0 <= b && b <= 1;
	}

	public Vector2 centre() {
		float D = 2f * (A.x * (B.y - C.y) + B.x * (C.y - A.y) + C.x * (A.y - B.y));
		float A2 = (A.x * A.x + A.y * A.y);
		float B2 = (B.x * B.x + B.y * B.y);
		float C2 = (C.x * C.x + C.y * C.y);
		float x = A2 * (B.y - C.y) + B2 * (C.y - A.y) + C2 * (A.y - B.y);
		float y = A2 * (C.x - B.x) + B2 * (A.x - C.x) + C2 * (B.x - A.x);
		return new Vector2(x, y) / D;
	}

	public override string ToString() {
		return "Triangle [" + A.ToString() + ", " + B.ToString() + ", " + C.ToString() + "]";
	}
}

}