using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System;

namespace geometrie {

public interface IPolygone {
	///<returns> true si ce polygone utilise le sommet p</returns>
	bool utilise(Vector2 p);

	///<summary>Accès au i-ième sommet du polygone</summary>
	Vector2 this[int i] { get; }

	///<summary>Cherche le sommet p parmi les sommets du polygone.</summary>
	int indexOf(Vector2 p);

	int indexOf(Vector2 s1, Vector2 s2);

	///<returns>l'aire du polygone</returns>
	float aire();

	int size();
}

public class Polygone : IPolygone, IEnumerable<Vector2> {
	protected readonly List<Vector2> _sommets;
	public List<int[]> _triangulation = null;

	public Polygone(IEnumerable<Vector2> sommets) {
		this._sommets = new List<Vector2>((from s in sommets select new Vector2(s.x, s.y)));
		Debug.Assert(this._sommets.Count >= 3);
	}

	public Polygone(Polygone polygone) {
		this._sommets = new List<Vector2>((from s in polygone._sommets select new Vector2(s.x, s.y)));
		this._triangulation = polygone._triangulation == null ? null : new List<int[]>(polygone._triangulation);
	}

	public int size() {
		return this._sommets.Count;
	}

	public bool utilise(Vector2 p) { return _sommets.Contains(p); }

	public bool contiens(Vector2 p) {
		int total = 0;
		for (int i = 0; i < this.size(); i++) {
			float div = this[(i + 1) % this.size()].y - this[i].y;
			if ((this[(i + 1) % this.size()].x >= p.x || this[i].x >= p.x) && System.Math.Abs(div) > 0.0001) {
				float a = (p.y - this[i].y) / div;
				if (0 <= a && a <= 1) total++;
			}
		}
		return total % 2 == 1;
	}

	public int indexOf(Vector2 p) { return _sommets.IndexOf(p); }

	public int indexOf(Vector2 s1, Vector2 s2) {
		int i = _sommets.IndexOf(s1);
		if (i >= 0 && _sommets[(i + 1) % _sommets.Count] == s2) return i;
		else return -1;
	}

	public Vector2 this[int index] {
		get {
			if (index < 0) {
				index += _sommets.Count;
			}
			if (index >= _sommets.Count)
				throw new ArgumentOutOfRangeException(index + " > " + _sommets.Count);
			return _sommets[index]; }
	}

	public Vector2 arete(int index) {
		return this[(index + 1) % this.size()] - this[index];
	}

	public float aire() {
		float total = 0;
		Vector2 u, v;
		for (int i = 0; i < this._sommets.Count - 1; i++) {
			u = this._sommets[i];
			v = this._sommets[(i + 1) % this._sommets.Count];
			total += u.x * v.y - v.x * u.y;
		}
		return total / 2;
	}

	public float perimetre() {
		float total = 0;
		for (int i = 1; i < this._sommets.Count; i++) {
			total += (this[i] - this[i - 1]).magnitude;
		}
		return total + (this[0] - this[-1]).magnitude;
	}

	public Vector2 min() {
		return new Vector2((from s in this._sommets select s.x).Min(),
			(from s in this._sommets select s.y).Min());
	}

	public Vector2 max() {
		return new Vector2((from s in this._sommets select s.x).Max(),
			(from s in this._sommets select s.y).Max());
	}

	public Vector2 centre() {
		Vector2 total = new Vector2();
		foreach (Vector2 sommet in this._sommets)
			total += sommet;
		return total / this._sommets.Count;
	}

	public IEnumerator<Vector2> GetEnumerator() {
		return _sommets.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator() {
		return _sommets.GetEnumerator();
	}

	public override string ToString()
	{
		return System.String.Format("P[{0}]", System.String.Join(", ", _sommets));
	}
	
	/** @brief Réduit la dimension du polygone.
	 * 
	 * Les bords du polygone son repoussés vers l'intérieur si la profondeur est positive.
	 * Le mouvement se fait le long de la normale à l'arête, la nouvelle arête est parallèle à l'originale.
	 * Les arêtes sont déplacées une par une vers l'intérieur en déplaçant les sommets le long des arêtes voisines,
	 * ainsi les arêtes voisines conservent leur direction.
	 * 
	 * Le déplacement des sommets peut entraîner la fusion de sommets en eux.
	 * L'algorithme se bloque pour ne pas réduire le polygone à moins de 3 sommets.
	 *
	 * @returns false en cas de trop grande compression du polygone
	 */
	public static bool reduction(Polygone original, float profondeur) {
		for (int bord = 0; bord < original.size(); bord++) {
			int sommet1 = (bord + original.size()) % original.size();
			int sommet2 = (bord + 1) % original.size();

			Vector2 arete = original[sommet2] - original[sommet1];
			Vector2 normal = new Vector2(-arete.y, arete.x).normalized;
			Vector2 prev_v, suiv_v;
			float prev_p, suiv_p;

			//Debug.LogFormat("Réduction bord {0} du polygone {1}\nnormal = {2}", bord, original, normal);

			float reste = profondeur; // distance restante à réduire.
			// Réduction de la surface
			while (reste > 0) {
				sommet1 = (bord + original.size()) % original.size();
				sommet2 = (bord + 1) % original.size();
				// Distance sur l'axe normal des points précédant et suivant
				prev_v = original[(sommet1 - 1) % original.size()] - original[sommet1];
				suiv_v = original[(sommet2 + 1) % original.size()] - original[sommet2];
				prev_p = Vector2.Dot(normal, prev_v);
				suiv_p = Vector2.Dot(normal, suiv_v);
				if (Mathf.Abs(prev_p) < 0.001 || Mathf.Abs(suiv_p) < 0.001) {
					//Debug.LogErrorFormat("polygone {0} donne {1} {2} sur le bord {3}", original, prev_p, suiv_p, bord);
					return false;
				}

				if (original.size() >= 3 && (prev_p < reste || suiv_p < reste)) {
					if (prev_p <= suiv_p) {
						reste -= prev_p;
						original._sommets[sommet2] += suiv_v * prev_p / suiv_p;
						// fusion du sommet 1 avec son précédant
						original._sommets.RemoveAt(sommet1);
						// Décaler l'indexage vers le précédant
						bord--;
					} else {
						reste -= suiv_p;
						original._sommets[sommet1] += prev_v * suiv_p / prev_p;
						// fusion du sommet 2 avec son successeur
						original._sommets.RemoveAt(sommet2);
						if (sommet2 == 0)
							bord--;
					}

					if (original._sommets.Count < 3) return false;
				} else {
					break;
				}
				//Debug.LogFormat("Réduction partielle: {0}", original);
			}

			sommet1 = (bord + original.size()) % original.size();;
			sommet2 = (bord + 1) % original.size();
			// Déplacer les sommets le long de leurs arêtes
			prev_v = original[(sommet1 - 1) % original.size()] - original[sommet1];
			suiv_v = original[(sommet2 + 1) % original.size()] - original[sommet2];
			prev_p = Vector2.Dot(normal, prev_v);
			suiv_p = Vector2.Dot(normal, suiv_v);
			if (Mathf.Abs(prev_p) < 0.001 || Mathf.Abs(suiv_p) < 0.001) {
				//Debug.LogErrorFormat("polygone {0} donne {1} {2} sur le bord {3}", original, prev_p, suiv_p, bord);
				return false;
			}

			original._sommets[sommet1] += prev_v * reste / prev_p;
			original._sommets[sommet2] += suiv_v * reste / suiv_p;
			//Debug.LogFormat("Réduction finale: {0}", original);
		}

		if (original._triangulation != null) {
			TriangulationDelaunay triangulation = new TriangulationDelaunay(original);
			original._triangulation = triangulation.indexTriangles();
		}

		return true;
	}
}

}