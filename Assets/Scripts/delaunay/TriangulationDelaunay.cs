using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace geometrie {

public class TriangulationDelaunay {

	private List<Vector2> _sommets;
	private List<Triangle> _triangles = new List<Triangle>();
	private Triangle super_triangle;

	public TriangulationDelaunay(IEnumerable<Vector2> points) {
		this._sommets = new List<Vector2>(points);

		const float epsilon = 10f;
		float x_min = (from s in this._sommets select s.x).Min() - epsilon;
		float x_max = (from s in this._sommets select s.x).Max() + epsilon;
		float y_min = (from s in this._sommets select s.y).Min() - epsilon;
		float y_max = (from s in this._sommets select s.y).Max() + epsilon;

		this.super_triangle = new Triangle(
			new Vector2(x_min, y_min),
			new Vector2(x_min + (2 * (x_max - x_min)), y_min),
			new Vector2(x_min, y_min + (2 * (y_max - y_min)))
		);
		this._triangles.Add(this.super_triangle);

		int i = 0;
		foreach (Vector2 p in this._sommets) {
			this.insertionPoint(p);
			i++;
			Debug.Assert(this._triangles.Count == i * 2 + 1, "au sommet " + i +" seulement "+this._triangles.Count+" triangles");
		}
		this.supprimerSuperTriangle();
	}

	public List<Vector2> sommets { get { return _sommets; } }
	public List<int[]> indexTriangles() {
		return new List<int[]>(from t in this._triangles select new int[3]{_sommets.IndexOf(t.A), _sommets.IndexOf(t.B), _sommets.IndexOf(t.C)});
	}

	public List<Triangle> triangles() {
		return new List<Triangle>(this._triangles);
	}

	private bool estDansCercle(Triangle t, Vector2 D) {
		Vector2 AD = t.A - D;
		Vector2 BD = t.B - D;
		Vector2 CD = t.C - D;

		Vector2 AD2 = t.A*t.A - D*D;
		Vector2 BD2 = t.B*t.B - D*D;
		Vector2 CD2 = t.C*t.C - D*D;

		float[,] matrice = new float[3,3];
		matrice[0, 0] = AD.x;

		float det = AD.x * BD.y * (CD2.x + CD2.y) +
					AD.y * (BD2.x + BD2.y) * CD.x +
					(AD2.x + AD2.y) * BD.x * CD.y -
					AD.x * (BD2.x + BD2.y) * CD.y -
					AD.y * BD.x * (CD2.x + CD2.y) -
					(AD2.x + AD2.y) * BD.y * CD.x;
		return det > 0;
	}

	private void insertionPoint(Vector2 p) {
		Debug.Assert(this.estDansCercle(this.super_triangle, p), p + " n'est pas dans " + this.super_triangle);
		List<Vector2> englobant = new List<Vector2>();
		List<Triangle> triangle_concurrents = new List<Triangle>(
			(from t2 in this._triangles where this.estDansCercle(t2, p) select t2)
		);

		Debug.Assert(triangle_concurrents.Count > 0, "Le point doit être intérieur à un cercle: " + p);
		if (triangle_concurrents.Count == 0) return;

		Triangle t = triangle_concurrents[0];
		int face = 0;
		while (englobant.Count < triangle_concurrents.Count + 2) {
			Triangle? t_op = (from t2 in this._triangles where t2.indexOf(t[(face + 1) % 3], t[face]) >= 0 select t2).FirstOrDefault();

			// Le triangle de l'autre côté doit aussi être supprimé
			if (t_op.HasValue && triangle_concurrents.Contains(t_op.Value)) {
				face = (t_op.Value.indexOf(t[(face + 1) % 3], t[face]) + 1) % 3;
				t = t_op.Value;
			} else {
				// ajout de la face
				// Le polygone englobant est construit en suivant les sommets
				englobant.Add(t[face]);
				// on passe sur la prochaine face.
				face = (face + 1) % 3;

				if (t[face] == englobant[0]) {
					break;
				}
			}
		}

		foreach (Triangle t_suppr in triangle_concurrents)
			this._triangles.Remove(t_suppr);

		Debug.Assert(englobant.Count >= 3);
		for (int i = 0; i < englobant.Count; i++) {
			this._triangles.Add(new Triangle(englobant[i], englobant[(i + 1) % englobant.Count], p));
		}
	}

	private void supprimerSuperTriangle() {
		this._triangles.Remove(this.super_triangle);
		for (int i = this._triangles.Count - 1; i >= 0; i--) {
			Triangle t = this._triangles[i];
			if (t.utilise(this.super_triangle.A) ||
				t.utilise(this.super_triangle.B) ||
				t.utilise(this.super_triangle.C)) {
					this._triangles.RemoveAt(i);
				}
		}
	}
}

}