using System.Collections.Generic;
using System.Linq;
using reseau;
using UnityEngine;
using ville;

/** @brief Classe de gestion d'un habitant de la ville.
 *
 * Un citoyen est localisé en extérieur ou dans un bâtiment.
 * Pendant la journée, les habitants vous chercher un objectif en extérieur et s'y rendre:
 * aller travailler, rentrer chez soi pour la nuit ou aller visiter un parc.
 */
public class Citoyen {
	private static int ID = 0;

	public int id;
	public Navigateur navigateur; //!< gestionaire de déplacement, enregistre la cible

	public Ville ville;
	public ville.ImmeubleResidentiel maison;
	public ville.ImmeubleBureau bureau = null;

	public ActionCitoyen activite = null; //!< Action/État actuelle du citoyen
	public ville.Batiment interieur = null; //!< indique si ce citoyen est en intérieur.

	public Citoyen(Ville ville, ville.ImmeubleResidentiel maison) {
		this.id = ++Citoyen.ID;
		this.ville = ville;
		this.maison = maison;
		this.interieur = this.maison;
		this.navigateur = new Navigateur(maison.centre, maison.centre);
	}
	
	public void Update() {
		if (this.bureau == null)
			this.trouverBureau();
		
		float heure = this.ville.horloge.heure();
		if (this.activite == null) {
			if (Random.Range(0f, 1f) < 0.5f) {
				Pause pause = new Pause();
				pause.duree = Horloge._heure * Random.Range(0.1f, 2f);
				this.activite = pause;
			} else if (this.bureau != null && this.interieur != this.bureau && heure > 6 && heure < 16) {
				//Debug.LogFormat("{0} va travailler.", this);
				this.activite = new Travailler();
			} else if (this.interieur != this.maison && (heure <= 6 || heure >= 20)) {
				//Debug.LogFormat("{0} rentre chez lui", this);
				this.activite = new Dormir();
			} else if (this.interieur == null || this.interieur.GetType() != typeof(ville.Batiment)) {
				//Debug.LogFormat("{0} cherche un parc", this);
				this.activite = new Detente();
			} else return;
			this.activite.citoyen = this;
			if (!this.activite.Start()) {
				//Debug.LogErrorFormat("Erreur d'activation de " + this.activite);
				Pause pause = new Pause();
				pause.citoyen = this;
				pause.duree = this.ville.horloge.total() + Horloge._heure * .5f;
				this.activite = pause;
				this.activite.Start();
			}
		}

		if (this.activite != null) {
			ActionResultat r = this.activite.onUpdate();
			if (r != ActionResultat.EN_COURS) {
				// Debug.LogFormat("Action {0} terminée: {1}", this.activite, r);
				this.activite = null;
			}
		}
	}

	/** @brief Entre dans un batiment.
	 */
	public void entrerDansBatiment(ville.Batiment batiment) {
		batiment.entreeCitoyen(this);
		this.interieur = batiment;
		Debug.LogFormat("{0} entré dans {1}", this, batiment);
	}

	public void quitterBatiment(Vector3 sortie) {
		this.interieur = null;
		this.navigateur.position = sortie;
		GameObject corps = GameObject.Instantiate(this.ville.GetComponent<Modeles>().citoyen, sortie, Quaternion.identity);
		corps.AddComponent(typeof(CitoyenComponent));
		corps.GetComponent<CitoyenComponent>().identite = this;
		corps.name = "Citoyen " + this.id;
	}

	private void trouverBureau() {
		List<ImmeubleBureau> bureau = this.ville.quartiers.SelectMany(q => q.immeubles)
			.OfType<ImmeubleBureau>()
			.Where(i => i.travailleures.Count < i.placesLibres()).ToList();
		if (bureau.Count > 0) {
			this.bureau = bureau[Random.Range(0, bureau.Count)];
			this.bureau.travailleures.Add(this);
		}
	}

	public bool trouverTrajet(ville.Batiment cible) {
		List<Vector3> departs;
		if (this.interieur != null) departs = new List<Vector3>(this.interieur.entrees());
		else {
			departs = new List<Vector3>();
			departs.Add(this.navigateur.position);
		}
		List<Vector3> trajet = this.navigateur.reseau.itineraire(
			departs,
			new List<Vector3>(cible.entrees()));
		if (trajet.Count == 0) {
			// Debug.Log("Pas de trajet vers " + cible);
			return false;
		}

		this.quitterBatiment(trajet[0]);
		// Debug.Log("Nouveau trajet ["+System.String.Join(",", trajet)+"] pour " + this);
		this.navigateur.setItineraire(trajet);
		return true;
	}


	public override string ToString() {
		return System.String.Format("Citoyen {0:d}, en {1}", this.id, this.navigateur.position);
	}
}

public enum ActionResultat {
	TERMINEE,
	ERREUR,
	ANNULEE,
	EN_COURS
};

public abstract class ActionCitoyen {
	public Citoyen citoyen;

	public virtual bool Start() { return true; }
	public abstract ActionResultat onUpdate();
}

public class ActionSequence : ActionCitoyen {
	protected int progression = -1;
	protected List<ActionCitoyen> sub = new List<ActionCitoyen>();

	protected virtual bool startSub() {
		this.sub[this.progression].citoyen = citoyen;
		return this.sub[this.progression].Start();
	}

	public override bool Start() {
		this.progression = 0;
		return this.startSub();
	}

	public override ActionResultat onUpdate() {
		if (this.progression == -1) return ActionResultat.ERREUR;
		ActionResultat r = this.sub[this.progression].onUpdate();
		if (r == ActionResultat.TERMINEE) {
			this.progression++;
			if (this.progression >= this.sub.Count) {
				this.progression = -1;
				return ActionResultat.TERMINEE;
			} else {
				this.startSub();
				return ActionResultat.EN_COURS;
			}
		} else return r;
	}
}

public class Pause : ActionCitoyen {
	public float duree = Horloge._heure;
	private float temps;

	public override bool Start() {
		this.temps = citoyen.ville.horloge.total() + duree;
		return base.Start();
	}

	public override ActionResultat onUpdate() {
		return citoyen.ville.horloge.total() < temps ? ActionResultat.EN_COURS : ActionResultat.TERMINEE;
	}
}

/** @brief Fait marcher la personne dans la rue.
 *
 * La personne suit les directives du navigateur.
 */
public class Marcher : ActionCitoyen {
	public override ActionResultat onUpdate() {
		if (citoyen.navigateur.arrive()) {
			return ActionResultat.TERMINEE;
		} else {
			// Debug.Log("Marche");
			citoyen.navigateur.avancer(0.2f);
			return ActionResultat.EN_COURS;
		}
	}
}

public class EntreeBatiment : ActionCitoyen {
	public Batiment batiment;

	public override ActionResultat onUpdate()
	{
		this.citoyen.entrerDansBatiment(this.batiment);
		return ActionResultat.TERMINEE;
	}
}

public class Travailler : ActionSequence {
	public Travailler() {
		this.sub.Add(new Marcher());
		this.sub.Add(new EntreeBatiment());
	}

	public override bool Start() {
		((EntreeBatiment) this.sub[1]).batiment = citoyen.bureau;
		if (!citoyen.trouverTrajet(citoyen.bureau)) return false;
		return base.Start();
	}
}

public class Dormir : ActionSequence {
	public Dormir() {
		this.sub.Add(new Marcher());
	}

	public override bool Start() {
		// Quitter le batiment
		if (!citoyen.trouverTrajet(citoyen.maison)) return false;
		return base.Start();
	}
}

public class Detente : ActionSequence {
	private Batiment jardin;

	public Detente() {
		this.sub.Add(new Marcher());
		this.sub.Add(new EntreeBatiment());
		this.sub.Add(new Pause());
	}

	public override bool Start() {
		List<Batiment> jardins = citoyen.ville.quartiers.SelectMany(q => q.immeubles)
				.Where(b => b.placesLibres() > 0)
				.OfType<ville.Batiment>().ToList();
		if (jardins.Count == 0) return false;

		this.jardin = jardins[Random.Range(0, jardins.Count)];
		((EntreeBatiment) this.sub[1]).batiment = this.jardin;
		if (!citoyen.trouverTrajet(jardin)) return false;
		return base.Start();
	}
}