﻿using UnityEngine;

///<summary> Génère des positions aléatoires sur un rectangle.
///La probabilité pour un point d'être choisi est proportionelle au bruit de perlin en ce point.
///</summary>
public class RandomPerlin
{
    public readonly int octaves;
    private readonly float freq_x, freq_y;
    private readonly float offset_x, offset_y;

    ///<summary>Créer un générateur aléatoire sur le rectangle entre les bornes.
    ///Les coefficients de bornes_min doivent être tous inférieurs à ceux de borne_max.
    ///</summary>
    public RandomPerlin(float freq = 0.1f, int octaves = 2) {
        Debug.Assert(freq != 0);
        this.freq_x = this.freq_y = freq;
        this.offset_x = this.offset_y = 18;
        this.octaves = octaves;
    }

    ///<summary>Retourne l'intensité du bruit de perlin au point</summary>
    ///<returns>Un nombre entre 0 et 1 (peu dépasser légèrement)</returns>
    public float intensite(Vector2 point) {
        float t = 0;
        for (int i = 0; i < this.octaves; i++)
            t += Mathf.PerlinNoise(point.x * this.freq_x * Mathf.Pow(2, i) + this.offset_x, point.y * this.freq_y * Mathf.Pow(2, i) + this.offset_y);
        return t / this.octaves;
    }

    ///<summary>Choisit un nouveau point aléatoirement entre les bornes définies.</summary>
    ///<returns>Un point entre borne_min et borne_max</returns>
    public Vector2 random(Vector2 borne_min, Vector2 borne_max) {
        do {
            Vector2 point = new Vector2(Random.Range(borne_min.x, borne_max.x),
                                        Random.Range(borne_min.y, borne_max.y));
            float p = this.intensite(point);
            if (Random.Range(0, 1) < p)
                return point;
        } while (true);
    }
}
