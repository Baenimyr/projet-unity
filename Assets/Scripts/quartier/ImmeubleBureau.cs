using System.Collections.Generic;
using geometrie;

namespace ville {

/** @brief Un immeuble de bureau ou vont les citoyens en journée.
 */
public class ImmeubleBureau : Immeuble {
	public List<Citoyen> travailleures = new List<Citoyen>();

	public ImmeubleBureau(Quartier quartier, Polygone zone, float hauteur)
		: base(quartier, zone, hauteur) {
	}
}

}