using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using geometrie;

namespace ville {

/**
 * @brief Un immeuble résidentiel abrittant des habitants la nuit.
 */
public class ImmeubleResidentiel : Immeuble {
	protected List<Citoyen> habitants = new List<Citoyen>();

	public ImmeubleResidentiel(Quartier quartier, Polygone zone, float hauteur)
	:base(quartier, zone, hauteur) {
		capacite = Mathf.FloorToInt(zone.aire() * quartier.ville.GetComponent<Configuration>().popResidentiel * hauteur);
		for (int i = 0; i < capacite; i++) {
			this.initCitoyen();
		}
		Debug.LogFormat("Création de {0:d} citoyens dans {1}", this.habitants.Count, this);
	}

	/** @brief Génère un nouveau citoyen pour le batiment. */
	private void initCitoyen() {
		Citoyen citoyen = new Citoyen(this.quartier.ville, this);
		this.habitants.Add(citoyen);
		this.occupants.Add(citoyen);
	}
}

}