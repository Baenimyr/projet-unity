using UnityEngine;
using geometrie;

namespace ville {
namespace quartier {

public class QuartierAffaire : Quartier {

	public QuartierAffaire(Ville ville, Polygone region)
		:base(ville, region) {}

	protected override ville.Batiment batimentPourTerrain(Polygone surface) {
		float hauteur = this.ville.perlin_population.intensite(surface.centre());
		if (Random.Range(0f, 1f) < 0.2f)
			return new Jardin(this, surface);
		else {
			Polygone sol_immeuble = new Polygone(surface);
			if (Polygone.reduction(sol_immeuble, 2f))
				return new ImmeubleBureau(this, sol_immeuble, 15 * hauteur);
			else return new Jardin(this, surface);
		}
	}
}

}
}