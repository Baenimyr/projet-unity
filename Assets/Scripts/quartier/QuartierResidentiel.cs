using UnityEngine;
using geometrie;
using System.Collections.Generic;

namespace ville {
namespace quartier {

public class QuartierResidentiel : Quartier {

	public QuartierResidentiel(Ville ville, Polygone region)
		:base(ville, region) {
	}
	
	protected override ville.Batiment batimentPourTerrain(Polygone surface) {
		float hauteur = this.ville.perlin_population.intensite(surface.centre());
		if (hauteur < 0.1f)
			return new Jardin(this, surface);
		else {
			Polygone sol_immeuble = new Polygone(surface);
			if (Polygone.reduction(sol_immeuble, 2f))
				return new ImmeubleResidentiel(this, sol_immeuble, 5 * hauteur);
			else return new Jardin(this, surface);
		}
	}
}

public class QuartierResidentielDense : QuartierResidentiel {

	public QuartierResidentielDense(Ville ville, Polygone region)
		:base(ville, region) {
	}

	protected override ville.Batiment batimentPourTerrain(Polygone surface) {
		float hauteur = this.ville.perlin_population.intensite(surface.centre());
		return new Immeuble(this, surface, 10 * hauteur);
	}

	protected override IEnumerable<Vector2> centresTerrains() {
		Vector2 region_min = this.region.min();
		Vector2 region_max = this.region.max();
		Polygone sub_region = new Polygone(this.region);
		Polygone.reduction(sub_region, 0.1f);

		int nb_terrains = (int)Mathf.Ceil(this.region.aire() / ville.GetComponent<Configuration>().surfaceBatiments);
		
		float perimetre = sub_region.perimetre();
		float pas = perimetre / nb_terrains;

		float offset = Random.Range(0, pas * 0.9f);
		for (int arete_id = 0; arete_id < sub_region.size(); arete_id++) {
			Vector2 normal = Vector2.Perpendicular(sub_region.arete(arete_id));
			float longueur = sub_region.arete(arete_id).magnitude;
			int i = 0;
			for (; (pas * i + offset) < longueur; i++) {
				float a = (pas * i + offset) / longueur;
				// Debug.LogFormat("quartier dense {0} {1}", i, a);
				yield return sub_region[arete_id] + a * sub_region.arete(arete_id) + normal * Random.Range(0.01f, 0.1f);
			}
			offset = (pas * i + offset) - longueur;
			//Debug.Log("offset " + offset);
		}
	}
}

}
}