using UnityEngine;
using System.Collections.Generic;
using geometrie;
using System.Linq;

namespace ville {

/// <summary>
/// Un quartier est une zone urbaine large dont les bâtiments ont des propriétés similaires.
/// </summary>
public class Quartier : Construction {
	public readonly Ville ville;
	/// <summary>Espace 2D occupé par le quartier</summary>
	public Polygone region;
	
	protected static float reduction = 1f;

	public List<Batiment> immeubles = new List<Batiment>();

	public Quartier(Ville ville, Polygone region) {
		this.region = region;
		this.ville = ville;

		TriangulationDelaunay triangulation = new TriangulationDelaunay(this.region);
		this.region._triangulation = triangulation.indexTriangles();
	}

	public override string ToString() {
		return System.String.Format("{0} {1}", this.GetType(), this.region.centre());
	}

	/** @brief Génère des points pour chaque terrains.
	 *
	 * Ces points doivent être compris dans le polygone de la région.
	 */
	protected virtual IEnumerable<Vector2> centresTerrains() {
		Vector2 region_min = this.region.min();
		Vector2 region_max = this.region.max();
		int nb_terrains = (int)Mathf.Ceil(this.region.aire() / ville.GetComponent<Configuration>().surfaceBatiments);

		while (0 < nb_terrains) {
			Vector2 point = new Vector2(Random.Range(region_min.x, region_max.x), Random.Range(region_min.y, region_max.y));
			if (this.region.contiens(point)) {
				nb_terrains--;
				yield return point;
			}
		}
	}

	protected virtual IEnumerator<Polygone> generationTerrains() {
		Vector2 region_min = this.region.min();
		Vector2 region_max = this.region.max();
		// Divise le quartier en bloc pour les batiments.
		List<Vector2> terrain_p = new List<Vector2>(this.centresTerrains());
		Delaunay.Voronoi voronoi = new Delaunay.Voronoi(terrain_p, null, new Rect(region_min, region_max - region_min));

		// Pour chaque région du diagramme de Voronoi
		// déterminer les propriétés du terrain et construire les bâtiments.
		foreach (Vector2 centre_block in terrain_p) {
			List<Vector2> t = voronoi.Region(centre_block);
			if (t.Count < 3) {
				//Debug.Log("region ignorée de " + t.Count +" éléments:" + System.String.Join(",", t));
				continue;
			}
			
			for (int i = 0; i < t.Count; i++) {
				Vector3 t_i = new Vector3(t[i].x, 0, t[i].y);
				if (t_i.x == region_min.x) {
					reseau.Route route1 = new reseau.Route(t_i, t_i + new Vector3(-Ville.largeur, 0, 0), 1);
					Ville.routes.ajoutRoute(route1);
					Ville.routes.ajoutRoute(route1.inverse());
				} else if (t_i.x == region_max.x) {
					reseau.Route route1 = new reseau.Route(t_i, t_i + new Vector3(Ville.largeur, 0, 0), 1);
					Ville.routes.ajoutRoute(route1);
					Ville.routes.ajoutRoute(route1.inverse());
				} else if (t_i.z == region_min.y) {
					reseau.Route route1 = new reseau.Route(t_i, t_i + new Vector3(0, 0, -Ville.largeur), 1);
					Ville.routes.ajoutRoute(route1);
					Ville.routes.ajoutRoute(route1.inverse());
				} else if (t_i.z == region_max.y) {
					reseau.Route route1 = new reseau.Route(t_i, t_i + new Vector3(0, 0, Ville.largeur), 1);
					Ville.routes.ajoutRoute(route1);
					Ville.routes.ajoutRoute(route1.inverse());
				} else {
					Vector3 t_i1 = new Vector3(t[(i + 1) % t.Count].x, 0, t[(i + 1) % t.Count].y);
					reseau.Route route = new reseau.Route(t_i, t_i1, 1);
					Ville.routes.ajoutRoute(route);
				}
			}

			//Debug.Log("immeuble sur le terrain à " + t.Count + " sommets");
			Polygone surface = new Polygone(t);
			if(!Polygone.reduction(surface, Quartier.reduction)) continue;
			yield return surface;
		}
	}

	protected virtual ville.Batiment batimentPourTerrain(Polygone surface) {
		return new Jardin(this, surface);
	}

	/// <summary>
	/// Divise le quartier en bloc correspondant chacun à un immeuble indépendant.
	/// </summary>
	public void init() {
		IEnumerator<Polygone> surfaces = this.generationTerrains();
		while (surfaces.MoveNext()) {
			Polygone surface = surfaces.Current;
			Batiment batiment = this.batimentPourTerrain(surface);
			this.immeubles.Add(batiment);
		}

		Debug.LogFormat("[Generation] Quartier {0}\n{1}\n{2} immeubles", this.ToString(), this.region.ToString(), this.immeubles.Count);
		foreach (Batiment batiment in this.immeubles)
			batiment.init();
	}

	public System.Collections.IEnumerator batir(Modeles modeles) {
		this.constructionSolQuartier(modeles);
		yield return null;
		foreach (Batiment terrain in this.immeubles) {
			yield return terrain.batir(modeles);
		}
	}

	private void constructionSolQuartier(Modeles modeles) {
		Vector2 centre = this.region.centre();
		int size = this.region.size();
		int triangles_base = size - 2;

		GameObject objet = GameObject.Instantiate(modeles.herbe, new Vector3(centre.x, -0.01f, centre.y), Quaternion.identity);
		objet.name = "Quartier base";
		Mesh sol = objet.GetComponent<MeshFilter>().mesh;
		Vector3[] vertices = new Vector3[size];
		int[] triangles = new int[triangles_base * 3];

		List<int[]> delaunay_triangles = this.region._triangulation;
		Debug.Assert(triangles_base == delaunay_triangles.Count,
			"Triangles " + triangles_base +" != " + delaunay_triangles.Count +"\n"+
			this.region + "=>" + System.String.Join(",", (from i in delaunay_triangles select "["+System.String.Join(",", i)+"]")));

		for (int i = 0; i < size; i++) {
			// Inversion pour avoir le sol orienté vers le haut
			vertices[i] = new Vector3(this.region[size - 1 - i].x - centre.x, 0, this.region[size - 1 - i].y - centre.y);
		}
		for (int i = 0; i < delaunay_triangles.Count; i++) {
			triangles[i*3 + 0] = delaunay_triangles[i][0];
			triangles[i*3 + 1] = delaunay_triangles[i][1];
			triangles[i*3 + 2] = delaunay_triangles[i][2];
		}

		sol.Clear();
		sol.vertices = vertices;
		sol.triangles = triangles;
		sol.RecalculateBounds();
		sol.RecalculateNormals();
		sol.RecalculateTangents();
	}

	public void Update() {
		foreach (Batiment batiment in this.immeubles) {
			batiment.Update();
		}
	}
}
}