using UnityEngine;

/// Incarnation physique d'une personne
[RequireComponent(typeof(Transform))]
public class CitoyenComponent : MonoBehaviour {
	public Citoyen identite;

	public void Update() {
		// En dehors d'un batiment support, les données de la personne sont mises à jour ici.
		if (this.identite == null) {
			GameObject.Destroy(this.gameObject);
			return;
		}
		this.identite.Update();
		this.actualiserTransform();
	}

	private void actualiserTransform() {
		if (this.identite.interieur != null) {
			// Détruire le modèle physique car l'objet est rentré dans un immeuble
			Debug.Log("Destruction du modèle");
			GameObject.Destroy(this.gameObject);
		} else {
			this.GetComponent<Transform>().localPosition = this.identite.navigateur.position + new Vector3(0, 0.5f, 0);
			if (this.identite.navigateur.route != null)
				this.GetComponent<Transform>().forward = this.identite.navigateur.route.vecteurDirecteur;
		}
	}
}