using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using geometrie;
using ville;

[RequireComponent(typeof(Configuration))]
[RequireComponent(typeof(Modeles))]
public class Ville : MonoBehaviour, Construction {
	public static readonly float largeur = 4f;

	// Génération
	public RandomPerlin perlin_population = new RandomPerlin(1f / 40);

	public List<ville.Quartier> quartiers = new List<ville.Quartier>();
	public static reseau.ReseauRoute routes = new reseau.ReseauRoute();

	private Vector2 pos_min, pos_max;
	private int I, J;
	private ville.Route grandesRoutes;

	[SerializeField]
	public GameObject soleil = null;
	[System.NonSerialized]
	public Horloge horloge = new Horloge(8 * Horloge._heure);

	private bool pret = false;

	public Ville() {}

	public void Awake() {
	}

	public void Start() {
		Vector3 p_min = this.GetComponent<Transform>().position - this.GetComponent<Transform>().localScale * 5;
		Vector3 p_max = this.GetComponent<Transform>().position + this.GetComponent<Transform>().localScale * 5;
		pos_min = new Vector2(p_min.x, p_min.z);
		pos_max = new Vector2(p_max.x, p_max.z);

		I = (int)Mathf.Floor((pos_max.x - pos_min.x) / this.GetComponent<Configuration>().largeur);
		J = (int)Mathf.Floor((pos_max.y - pos_min.y) / this.GetComponent<Configuration>().largeur);

		this.init();
		//this.ville.batir(GetComponent<Modeles>());
		this.StartCoroutine("BatirVille");
	}

	public IEnumerator BatirVille() {
		yield return this.batir(GetComponent<Modeles>());
		this.pret = true;
		Debug.LogFormat("Reseau de routes avec {0} routes", routes.Count);
	}

	public void init() {
		this.assemblageRoutes();
		this.assemblageQuartiers();
		foreach (ville.Quartier quartier in this.quartiers)
			quartier.init();
	}

	public System.Collections.IEnumerator batir(Modeles modeles) {
		yield return this.grandesRoutes.batir(modeles);
		foreach (Quartier quartier in this.quartiers) {
			yield return quartier.batir(modeles);
		}
	}

	///<summary>Utilise Voronoi pour générer les contours des quartiers</summary>
	private void assemblageQuartiers() {
		for (int i = 0; i < I; i++) {
			for (int j = 0; j < J; j++) {
				Vector2 b = new Vector2(i, j) * this.GetComponent<Configuration>().largeur + this.pos_min;

				Vector2[] region = new Vector2[] {
					b + new Vector2(largeur, largeur),
					b + new Vector2(this.GetComponent<Configuration>().largeur - largeur, largeur),
					b + new Vector2(this.GetComponent<Configuration>().largeur - largeur, this.GetComponent<Configuration>().largeur - largeur),
					b + new Vector2(largeur, this.GetComponent<Configuration>().largeur - largeur)
				};
				
				Polygone polygone = new Polygone(region);
				float random_quartier = Random.Range(0f, 1f);
				ville.Quartier quartier;
				/*if (random_quartier > 0.75f)
					quartier = new ville.quartier.QuartierResidentielDense(this, polygone);
				else*/ if (random_quartier > 0.3f)
					quartier = new ville.quartier.QuartierResidentiel(this, polygone);
				else if (random_quartier > 0.2f)
					quartier = new ville.quartier.QuartierAffaire(this, polygone);
				else
					quartier = new ville.Quartier(this, polygone);
				this.quartiers.Add(quartier);
			}
		}
	}

	private void assemblageRoutes() {
		Vector3 pos_min = new Vector3(this.pos_min.x, 0, this.pos_min.y);
		Maillage routes_mesh = new Maillage();
		Vector3[,,] carrefours = new Vector3[I + 1, J + 1, 4];

		for (int i = 0; i <= I; i++)
			for (int j = 0; j <= J; j++) {
				Vector3 angle = new Vector3(i, 0, j) * this.GetComponent<Configuration>().largeur + pos_min;

				carrefours[i, j, 0] =	new Vector3(-largeur, 0, -largeur) + angle;
				carrefours[i, j, 1] =	new Vector3(largeur, 0, -largeur) + angle;
				carrefours[i, j, 2] =	new Vector3(largeur, 0, largeur) + angle;
				carrefours[i, j, 3] =	new Vector3(-largeur, 0, largeur) + angle;

				routes_mesh.ajoutTriangle(carrefours[i, j, 0], carrefours[i, j, 2], carrefours[i, j, 1]);
				routes_mesh.ajoutTriangle(carrefours[i, j, 0], carrefours[i, j, 3], carrefours[i, j, 2]);

				routes.ajoutRoute(new reseau.Route(angle + new Vector3(largeur, 0, 0), angle + new Vector3(0, 0, largeur), 1));
				routes.ajoutRoute(new reseau.Route(angle + new Vector3(0, 0, largeur), angle + new Vector3(-largeur, 0, 0), 1));
				routes.ajoutRoute(new reseau.Route(angle + new Vector3(-largeur, 0, 0), angle + new Vector3(0, 0, -largeur), 1));
				routes.ajoutRoute(new reseau.Route(angle + new Vector3(0, 0, -largeur), angle + new Vector3(largeur, 0, 0), 1));
			}

		for (int i = 0; i <= I; i++)
			for (int j = 0; j <= J; j++) {
				Vector3 angle = new Vector3(i, 0, j) * this.GetComponent<Configuration>().largeur + pos_min;
				Vector3 angle_nord = angle + new Vector3(this.GetComponent<Configuration>().largeur, 0, 0);
				Vector3 angle_ouest = angle + new Vector3(0, 0, this.GetComponent<Configuration>().largeur);

				if (i < I) {
					Vector3[] route_nord = new Vector3[] {
						carrefours[i, j, 2],
						carrefours[i, j, 1],
						carrefours[i + 1, j, 0],
						carrefours[i + 1, j, 3]
					};

					routes_mesh.ajoutTriangle(route_nord[0], route_nord[2], route_nord[1]);
					routes_mesh.ajoutTriangle(route_nord[0], route_nord[3], route_nord[2]);

					// this.route_nord_sud[i, j] = new ville.Route(this, new Polygone(route_nord), System.String.Format("route nord {0} {1}", i, j));
					reseau.Route route = new reseau.Route(angle + new Vector3(largeur, 0, 0),
						angle + new Vector3(this.GetComponent<Configuration>().largeur, 0, 0) + new Vector3(-largeur, 0, 0), 1);
					routes.ajoutRoute(route);
					routes.ajoutRoute(route.inverse());
				}

				if (j < J) {
					Vector3[] route_ouest = new Vector3[] {
						carrefours[i, j, 3],
						carrefours[i, j, 2],
						carrefours[i, j + 1, 1],
						carrefours[i, j + 1, 0]
					};
					routes_mesh.ajoutTriangle(route_ouest[0], route_ouest[2], route_ouest[1]);
					routes_mesh.ajoutTriangle(route_ouest[0], route_ouest[3], route_ouest[2]);

					//this.route_ouest_est[i, j] = new ville.Route(this, new Polygone(route_ouest), System.String.Format("route ouest {0} {1}", i, j));
					reseau.Route route = new reseau.Route(angle + new Vector3(0, 0, largeur),
						angle + new Vector3(0, 0, this.GetComponent<Configuration>().largeur) + new Vector3(0, 0, -largeur), 1);
					routes.ajoutRoute(route);
					routes.ajoutRoute(route.inverse());
				}
			}

		this.grandesRoutes = new ville.Route(this, routes_mesh, "Voies Rapides");
	}

	public void Update() {
		if (!this.pret) return;
		this.horloge.FixedUpdate();

		if (this.soleil != null && this.soleil.GetComponent<UnityEngine.Light>() != null) {
			this.soleil.GetComponent<Transform>().rotation = Quaternion.Euler(this.horloge.jour() * 360 - 90, 0, 0);
			if (this.horloge.heure() > 6 && this.horloge.heure() < 18) {
				this.soleil.GetComponent<Light>().intensity = -Mathf.Cos(this.horloge.jour() * 2 * Mathf.PI);
			} else {
				this.soleil.GetComponent<Light>().intensity = 0f;
			}
		}

		foreach(Quartier quartier in this.quartiers)
			try {
				quartier.Update();
			} catch (System.Exception e) {
				Debug.LogError(e);
				this.pret = false;
				return;
			}
	}
}