using UnityEngine;

public class Horloge {
	public static float vitesse = 60;
	public static int _jour = 84600;
	public static int _heure = 3600;

	protected float temps;

	public Horloge(float temps = 0) {
		this.temps = temps;
	}

	public float total() {
		return this.temps;
	}

	///<summary> nombre de jours depuis le début </summary>
	public float jour() {
		return (float) this.temps / _jour;
	}

	///<summary> heure du jour entre 0 et 24 exclus. </summary>
	public float heure() {
		return (float) (this.temps % _jour) / _jour * 24;
	}

	public void FixedUpdate() {
		this.temps += Time.deltaTime * Horloge.vitesse;
	}
}