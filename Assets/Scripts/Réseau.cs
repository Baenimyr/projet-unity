using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace reseau {

/** Navigateur pour les entités.
 *
 */
public class Navigateur {
	public ReseauRoute reseau = Ville.routes;

	public Vector3 position;
	public Vector3 cible;
	public Queue<Vector3> itineraire; //!< sommets de passage dans le réseau

	public Route route = null; //!< Route utilisée par le navigateur
	public int voie = 0;    //!< voie sur la route
	public float progression; //!< distance restante jusqu'à l'extrémité de la rue.

	public Navigateur(Vector3 position, Vector3 cible) {
		this.position = position;
		this.cible = cible;
		this.itineraire = new Queue<Vector3>();
	}

	/** Arrivé à destination. */
	public bool arrive() {
		return this.itineraire.Count == 0 && (this.progression == 0 || (this.position - this.cible).sqrMagnitude < 0.01f);
	}

	public void setItineraire(IEnumerable<Vector3> points) {
		this.itineraire = new Queue<Vector3>(points);
		this.cible = this.itineraire.Dequeue();
		this.progression = (this.position - this.cible).magnitude;
	}

	public void avancer(float distance) {
		while (distance > 0) {
			if ((this.position - this.cible).sqrMagnitude < 0.01) {
				this.progression = 0;
				this.position = this.cible;

				if (this.itineraire.Count == 0)
					return;
				else this.cible = this.itineraire.Dequeue();
			}

			// Debug.LogFormat("Route '{0}' pour position:{1}, cible:{2}, progression:{3}", this.route, this.position, this.cible, this.progression);
			if (this.route != null && this.progression == 0) {
				this.route.quitterCirculation(this);
				this.route = null;
				// Change de route support
				if (this.itineraire.Count > 0) {
					this.cible = this.itineraire.Dequeue();
				} else {
					return;
				}
			}

			if (this.route == null) {
				this.route = reseau.routeEntre(this.position, this.cible);
				if (this.route == null) {
					this.route = new Route(this.position, this.cible, 1);
				}
				this.route.rejoindreCirculation(this, this.position);
			}
			distance -= this.route.avancer(this, distance);
			this.position = this.route.position(this);
			// Debug.LogFormat("maintenant en {0}, progression={1}, distance = {2}", this.position, this.progression, distance);
		}
	}
}

/** @brief Une route à sens unique.
 * 
 * La route contient n voies.
 * Chaque véhicule sur la route à une progression et une voie assignée,
 * il ne peut pas dépasser les véhicules sur la même voie que lui mais peu changer de voies.
 * La progression va en décroissant jusqu'au prochain carrefour
 */
public class Route {
	public readonly Vector3 p1, p2;
	///<summary>Nombre de voie dans un sens.</summary>
	public readonly int nb_voies;

	private List<Navigateur>[] vehicules;

	public Route(Vector3 p1, Vector3 p2, int nb_voies) {
		this.p1 = p1;
		this.p2 = p2;
		this.nb_voies = nb_voies;
		this.vehicules = new List<Navigateur>[nb_voies];
		for (int i = 0; i < nb_voies; i++)
			this.vehicules[i] = new List<Navigateur>();
	}

	public float longueur() {
		return (p2 - p1).magnitude;
	}

	public Vector3 vecteurDirecteur {
		get { return (p2 - p1).normalized; }
	}

	public float progression(Vector3 position) {
		return Vector3.Dot(p2 - position, this.vecteurDirecteur);// / this.longueur();
	}

	/** Vecteur le plus court entre la route et la position. */
	public Vector3 distance(Vector3 p) {
		Vector3 vec = p - p1;
		float s = Vector3.Dot(vec, this.vecteurDirecteur);
		if (s < 0) return vec;
		else if (s > this.longueur()) return p - p2;
		else return vec - s * this.vecteurDirecteur;
	}

	public Vector3 position(Navigateur v) {
		return p2 - this.vecteurDirecteur * v.progression - Vector3.Cross(this.vecteurDirecteur, new Vector3(0, 1, 0)) * (0.5f + v.voie);
	}

	/** @brief Cherche quelle est la progression du véhicule précédant.
	 * 
	 * Permet d'interroger si un véhicule peut avancer ou changer de voie sans entrée en collision avec un autre véhicule.
	 */
	public float disponibilite(int voie, float progression) {
		float? p = (from v in this.vehicules[voie] where v.progression < progression select v.progression).Max();
		return p.HasValue ? p.Value : 0;
	}

	public void rejoindreCirculation(Navigateur vehicule, Vector3 position) {
		vehicule.progression = this.progression(position);
		this.insertionVoie(vehicule, 0, vehicule.progression);
	}

	/** Permet de quitter la circulation, peut importe la voie. */
	public void quitterCirculation(Navigateur vehicule) {
		foreach (List<Navigateur> voie in this.vehicules)
			if (voie.Remove(vehicule))
				break;
	}

	/** @brief Insert un véhicule dans la voie.
	 *
	 * Cette fonction peut être utilisée pour changer de voie.
	 */
	public void insertionVoie(Navigateur v, int voie, float progression) {
		voie = Mathf.Min(this.nb_voies - 1, voie);
		int index = this.vehicules[voie].FindIndex((Navigateur v1)=>{ return v1.progression > progression; });
		if (index != -1)
			this.vehicules[voie].Insert(index, v);
		else
			this.vehicules[voie].Add(v);
	}

	/** @brief Fait avancer le véhicule d'au plus `distance`.
	 *
	 * Le véhicule peut avancer jusqu'au carrefour et en préservant la distance de sécurité avec le véhicule
	 * précédant. Ce mécanisme empêche la superposition des véhicules.
	 * @return la distance parcourue.
	 */
	public float avancer(Navigateur v, float distance) {
		int devant = this.vehicules[v.voie].IndexOf(v) - 1;
		// TODO: ajouter changement de voies si possible pour doubler et se rabattre
		if (devant < 0) {
			distance = Mathf.Min(distance, v.progression);
		} else {
			distance = Mathf.Min(distance, Mathf.Max(v.progression - this.vehicules[v.voie][devant].progression - 1f, 0)); // et longueur du véhicule devant
		}
		v.progression -= distance;
		return distance;
	}

	public override bool Equals(object obj)
	{
		if (obj == null || !this.GetType().Equals(obj.GetType()))
			return false;
		else return p1 == ((Route) obj).p1 && p2 == ((Route) obj).p2;
	}	

	public override int GetHashCode() {
		return (this.p1.GetHashCode() << 4) ^ this.p2.GetHashCode();
	}

	public Route inverse() {
		return new Route(this.p2, this.p1, this.nb_voies);
	}

	public override string ToString() {
		return System.String.Format("Route {0} => {1}", this.p1, this.p2);
	}
}

/** @brief Le réseau de routes et chemin en ville.
 */
public class ReseauRoute {
	private HashSet<Route> routes = new HashSet<Route>();
	private Dictionary<Vector3, List<Route>> carrefours = new Dictionary<Vector3, List<Route>>();

	public int Count {
		get { return this.routes.Count; }
	}

	/** Crée un nouveau carrefour.
	 * Si des routes intersecte ce carrefour, elles seront divisées en deux pour créer une connexion.
	 */
	private void nouveauCarrefour(Vector3 pos) {
		if (this.carrefours.ContainsKey(pos)) return;

		this.carrefours[pos] = new List<Route>();
		List<Route> nouveaux = new List<Route>();
		List<Route> suppression = new List<Route>();
		foreach (Route route in this.routes) {
			Vector3 diff = pos - route.p1;
			float s = Vector3.Dot(diff, route.vecteurDirecteur);
			if (0.01f <= s && s + 0.01f < route.longueur() && (diff - route.vecteurDirecteur * s).sqrMagnitude < 0.01f) {
				Debug.LogFormat("Route {0} coupée à {1}", route, pos);
				nouveaux.Add(new Route(route.p1, pos, route.nb_voies));
				nouveaux.Add(new Route(pos, route.p2, route.nb_voies));
			}
		}

		suppression.ForEach(r => {
			this.carrefours[r.p1].Remove(r);
			this.routes.Remove(r);
		});
		nouveaux.ForEach(r => this.ajoutRoute(r));
	}
	
	public void ajoutRoute(Route route) {
		if (!this.routes.Contains(route)) {
			this.nouveauCarrefour(route.p1);
			this.nouveauCarrefour(route.p2);
			this.routes.Add(route);
			this.carrefours[route.p1].Add(route);
		}
	}

	/** @brief Cheche la route la plus proche à rejoindre.
	 * 
	 * Peut renvoyer null.
	 */
	public Route routeProche(Vector3 point) {
		return this.routes.OrderBy(r => r.distance(point).sqrMagnitude).First();
	}

	/** Cherche une route qui relie strictement ces 2 points. */
	public Route routeEntre(Vector3 p1, Vector3 p2) {
		return this.routes.Where(r => r.p1 == p1 && r.p2 == p2).FirstOrDefault();
	}

	public List<Vector3> itineraire(List<Vector3> departs, List<Vector3> arrivees) {
		HashSet<Vector3> nouveaux = new HashSet<Vector3>();
		Dictionary<Vector3, (float, Vector3)> distances = new Dictionary<Vector3, (float, Vector3)>();
		// Graphe temporaire pour rejoindre les routes
		Dictionary<Vector3, List<Vector3>> temp = new Dictionary<Vector3, List<Vector3>>();

		foreach (Vector3 d in departs) {
			distances[d] = (0, d);

			Route entree = this.routeProche(d);
			Vector3 projection = entree.distance(d);
			Vector3 p = d - projection;
			if(!temp.ContainsKey(d)) temp[d] = new List<Vector3>();
			if(!temp.ContainsKey(p)) temp[p] = new List<Vector3>();
			if(!temp.ContainsKey(p)) temp[p] = new List<Vector3>();
			temp[d].Add(p);
			temp[p].Add(entree.p1);
			temp[p].Add(entree.p2);
			nouveaux.Add(d);
		}

		foreach (Vector3 d in arrivees) {
			Route entree = this.routeProche(d);
			Vector3 projection = entree.distance(d);
			Vector3 p = d - projection;
			if(!temp.ContainsKey(p)) temp[p] = new List<Vector3>();
			if(!temp.ContainsKey(entree.p1)) temp[entree.p1] = new List<Vector3>();
			if(!temp.ContainsKey(entree.p2)) temp[entree.p2] = new List<Vector3>();
			temp[p].Add(d);
			temp[entree.p1].Add(p);
			temp[entree.p2].Add(p);
		}

		while (nouveaux.Count > 0 && arrivees.Select(a => nouveaux.Contains(a) || !distances.ContainsKey(a)).Any()) {
			Vector3 sommet = nouveaux.OrderBy(n => distances[n].Item1).First();
			nouveaux.Remove(sommet);

			if (temp.ContainsKey(sommet)) {
				foreach (Vector3 p in temp[sommet]) {
					float distance = (p - sommet).magnitude;
					if (!distances.ContainsKey(p) || distances[p].Item1 > distances[sommet].Item1 + distance) {
						distances[p] = (distances[sommet].Item1 + distance, sommet);
						if(!nouveaux.Contains(p)) nouveaux.Add(p);
					}
				}
			}
			if (this.carrefours.ContainsKey(sommet)) {
				foreach (Route route in this.carrefours[sommet]) {
					float distance = (route.p2 - sommet).magnitude;
					if (!distances.ContainsKey(route.p2) || distances[route.p2].Item1 > distances[sommet].Item1 + distance) {
						distances[route.p2] = (distances[sommet].Item1 + distance, sommet);
						if(!nouveaux.Contains(route.p2)) nouveaux.Add(route.p2);
					}
				}
			}
		}

		List<Vector3> atteintes = arrivees.Where(a => distances.ContainsKey(a)).OrderBy(a => distances[a].Item1).ToList();
		if (atteintes.Count > 0) {
			Debug.Assert(distances.ContainsKey(atteintes[0]));
			List<Vector3> itineraire = new List<Vector3>();
			itineraire.Add(atteintes[0]);
			while (itineraire[0] != distances[itineraire[0]].Item2) {
				itineraire.Insert(0, distances[itineraire[0]].Item2);
			}
			return itineraire;
		} else {
			return new List<Vector3>();
		}
	}
}

}