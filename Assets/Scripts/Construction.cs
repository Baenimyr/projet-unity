using UnityEngine;
using geometrie;
using System.Collections.Generic;
using System.Linq;

namespace ville {

public interface Construction {
	void init();
	/// <summary>Construit le GameObject</summary>
	System.Collections.IEnumerator batir(Modeles modeles);
}

/// <summary>Un terrain à batir
/// <par>Le terrain est un polygone (toujours convexe) dans lequel peut être construit
/// un immeuble, des bureaux ou un parc. Dans les zones faiblement peuplés cela peut être un champs.</par>
/// </summary>
public abstract class Batiment : Construction {
	public readonly Quartier quartier;
	public Polygone surface;
	public Vector3 centre;
	public float hauteur;

	protected int capacite= 0;
	protected List<Citoyen> occupants = new List<Citoyen>(); //!< personne présents dans le batiment.

	protected Batiment(Quartier quartier, Polygone zone, float hauteur) {
		this.quartier = quartier;
		this.surface = zone;
		this.hauteur = hauteur;
		Vector2 centre = zone.centre();
		this.centre = new Vector3(centre.x, 0, centre.y);

		TriangulationDelaunay triangulation = new TriangulationDelaunay(this.surface);
		this.surface._triangulation = triangulation.indexTriangles();
	}

	public void init() {}

	public abstract System.Collections.IEnumerator batir(Modeles modeles);

	/** nombre de places libres dans le batiment. */
	public int placesLibres() {
		return this.capacite - this.occupants.Count;
	}

	/** @brief Liste des accès au batiment.
	 *
	 * Par défaut, les accès sont situés aux extrémités des faces du bâtiments.
	 */
	public virtual IEnumerable<Vector3> entrees() {
		for (int i=0; i < this.surface.size(); i++) {
			yield return new Vector3(this.surface[i].x, 0, this.surface[i].y);
		}
	}

	/** @brief Fait entrer un citoyen dans le batiment.
	 *
	 * Le GameObject lié peut être effacé dans le cas de batiments fermés.
	 * @return false si la capacité du batiment ne le permet pas.
	 */
	public virtual bool entreeCitoyen(Citoyen citoyen) {
		if (this.capacite < this.occupants.Count) {
			this.occupants.Add(citoyen);
			// TODO: détruire le GameObject
			return true;
		} else return false;
	}

	public void Update() {
		for (int i = this.occupants.Count - 1; i >= 0; i--) {
			if (occupants[i].interieur != this)
				this.occupants.RemoveAt(i);
			else
				occupants[i].Update();
		}
	}
};

public class Route : Construction {
	public readonly Ville ville;
	public readonly string nom;
	protected Maillage surface;

	public Route(Ville ville, Maillage zone, string nom) {
		this.ville = ville;
		this.nom = nom;
		this.surface = zone;
	}

	public void init() {}
	
	public System.Collections.IEnumerator batir(Modeles modeles) {
		GameObject objet = GameObject.Instantiate(modeles.route, new Vector3(0, 0, 0), Quaternion.identity);
		objet.name = this.nom;

		Mesh mesh = objet.GetComponent<MeshFilter>().mesh;
		this.surface.apply(mesh);

		Vector2[] uv = new Vector2[this.surface.Count];
		for (int i = 0; i < this.surface.Count; i++) {
			Vector3 sommet = this.surface.sommet(i);
			uv[i] = new Vector2(sommet.x, sommet.z);
		}
		mesh.uv = uv;

		// Debug.Log("Mesh de " + mesh.vertices.Length +" sommets et "+mesh.triangles.Length/3+" triangles");
		return null;
	}
}

public class Jardin : Batiment {

	public Jardin(Quartier quartier, Polygone zone)
		: base(quartier, zone, 0) {
		this.capacite = Mathf.FloorToInt(this.surface.aire() / 20);
	}

	public override System.Collections.IEnumerator batir(Modeles modeles) {
		Vector3[] vertices = new Vector3[this.surface.size()];
		int[] triangles = new int[this.surface._triangulation.Count * 3];
		Vector2[] textures = new Vector2[this.surface.size()];

		for (int i = 0; i < this.surface.size(); i++) {
			vertices[i] = new Vector3(this.surface[i].x, 0, this.surface[i].y) - this.centre;
			textures[i] = this.surface[i] - new Vector2(this.centre.x, this.centre.z);
		}
		for (int i = 0; i < this.surface._triangulation.Count; i++) {
			triangles[i * 3] = this.surface._triangulation[i][0];
			triangles[i * 3 + 1] = this.surface._triangulation[i][2];
			triangles[i * 3 + 2] = this.surface._triangulation[i][1];
		}

		GameObject objet = GameObject.Instantiate(modeles.herbe, this.centre, Quaternion.identity);
		objet.name = "parc";

		Mesh mesh = objet.GetComponent<MeshFilter>().mesh;
		mesh.Clear();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = textures;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();

		this.ajoutArbres(modeles);
		yield return null;
	}

	private void ajoutArbres(Modeles modeles) {
		if (modeles.arbre == null) return;

		RandomPerlin random = new RandomPerlin();

		int quantite = Mathf.CeilToInt(this.surface.aire() / 100);
		for (int i = 0; i < quantite;) {
			Vector2 point = random.random(this.surface.min(), this.surface.max());
			if (this.surface.contiens(point)) {
				GameObject arbre = GameObject.Instantiate(modeles.arbre, new Vector3(point.x, 0, point.y), Quaternion.Euler(0, Random.Range(0f, 3f), 0));
				i++;
			}
		}
	}

	/** Pour les parcs, les accès sont situés au milieu des bords. */
	public override IEnumerable<Vector3> entrees() {
		for (int i=0; i < this.surface.size(); i++) {
			Vector2 mid = (this.surface[(i + 1) % this.surface.size()] + this.surface[i]) / 2;
			yield return new Vector3(mid.x, 0, mid.y);
		}
	}
};


public class Immeuble : Batiment {

	public Immeuble(Quartier quartier, Polygone zone, float hauteur)
		: base(quartier, zone, hauteur) {
		this.capacite = (int)(zone.aire() * hauteur);
	}

	public override System.Collections.IEnumerator batir(Modeles modeles) {
		Polygone surface = new Polygone(this.surface);

		int size = surface.size();
		int triangles_base = surface._triangulation.Count;

		Vector3[] vertices = new Vector3[surface.size() * 2];
		int[] triangles = new int[(surface._triangulation.Count * 2 + surface.size() * 2) * 3];

		for (int i = 0; i < surface.size(); i++) {
			vertices[i] = new Vector3(surface[i].x, 0, surface[i].y) - this.centre;
			vertices[i + surface.size()] = new Vector3(surface[i].x, hauteur, surface[i].y) - this.centre;
		}

		// Triangulation en éventail des faces hautes et basses
		for (int i = 0; i < surface._triangulation.Count; i++) {
			triangles[i*3 + 0] = 0;
			triangles[i*3 + 1] = i + 2;
			triangles[i*3 + 2] = i + 1;
			triangles[(triangles_base + i)*3 + 0] = size;
			triangles[(triangles_base + i)*3 + 1] = size + i + 2;
			triangles[(triangles_base + i)*3 + 2] = size + i + 1;
		}

		for (int i = 0; i < size; i++) {
			// triangles_base * 2 * 3: triangles des bases
			triangles[triangles_base*6 + i*6 + 0] = i;
			triangles[triangles_base*6 + i*6 + 1] = i + size;
			triangles[triangles_base*6 + i*6 + 2] = (i + 1) % size;
			triangles[triangles_base*6 + i*6 + 3] = i + size;
			triangles[triangles_base*6 + i*6 + 4] = (i + 1) % size + size;
			triangles[triangles_base*6 + i*6 + 5] = (i + 1) % size;
		}

		GameObject objet = GameObject.Instantiate(modeles.batiment_bas, this.centre, Quaternion.identity);
		objet.name = "immeuble";

		Mesh mesh = objet.GetComponent<MeshFilter>().mesh;
		mesh.Clear();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		yield return this;
	}
};

}