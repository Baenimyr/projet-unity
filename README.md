# Projet _City Simulation_
----

## Ville
La ville est délimitée par un _plane_.
Les quartiers ne se génèrent que sur cette surface.

La ville possède une horloge qui permet de contrôler le mouvement du soleil et cadencer l'activité des habitants.

Les rues forment un graphe orienté sur lequel se déplace les citoyens.
Les véhicules doivent se placer sur l'une des voies de circulation et respecter une distance de sécurité avec le véhicule qui les précèdes.
Il y en théorie plusieurs voies disponibles sur lesquels circuler, mais le changement de voies en cas de bouchon n'est pas implémenté.
L'objectif aurait été de réaliser le système de guidage utilisé dans le dessin-animé _Wall-E_

## Quartier
La ville est divisée en quartier de dimensions carrés.
Entre chaque quartier, s'étendent des avenues larges qui permettent le mouvement de beaucoup de citoyens en même temps.
Le quartier peuvent être de 3 catégories: résidentiels, d'affaires ou _campagnes_.

Les terrains sont le résultat d'un diagramme de Voronoï avec des points aléatoires dans la surface.
Le nombre de terrains permet d'obtenir, en moyenne, un surface de bâtiments configurable avec l'option `surfaceBatiments`.
La place pour la rue est dégagée en réduisant le polygone.

### Quartier résidentiels
Le quartier résidentiel est composé de petites maisons avec beaucoup de place entre les bâtiments.

### Quartier d'affaire
Le quartier d'affaire contient des immeubles de bureaux où doivent aller les citoyens en journée pour travailler.
Quelques parcs disparates apportent un peu d'espace.

### Jardin
Les jardins sont des terrains sans bâtiments mais des arbres où les citoyens peuvent passer le temps.


## Citoyen
Les citoyens peuvent réaliser plusieurs objectifs.
Ces actions sont gérées par un petit arbre de comportement implémenté dans le fichier `Scripts/Citoyen.cs`.

Par exemple, l'action de se détendre consiste en: chercher un parc, se déplacer jusqu'à ce parc, entrer dans le bâtiment.
Dans le cas où aucune action n'est possible ou pour étaler l'activité de la ville dans le temps, les citoyens peuvent se mettre en **pause** entre 5 minutes et 2 heures.